package com.visa.ncg.canteen;

public class AccountResponse {

  private long id;
  private int balance;
  private String name;

  public AccountResponse(long id, int balance, String name) {
    this.id = id;
    this.balance = balance;
    this.name = name;
  }

  public AccountResponse(Account account) {
    this(account.getId(), account.balance(), account.name());
  }

  public AccountResponse() {
  }

  // Alternative to constructor: a "factory method"
  public static AccountResponse fromAccount(Account account) {
    AccountResponse accountResponse = new AccountResponse();
    accountResponse.setId(account.getId());
    accountResponse.setBalance(account.balance());
    accountResponse.setName(account.name());
    return accountResponse;
  }

  public long getId() {
    return id;
  }

  public int getBalance() {
    return balance;
  }

  public String getName() {
    return name;
  }

  public void setId(long id) {
    this.id = id;
  }

  public void setBalance(int balance) {
    this.balance = balance;
  }

  public void setName(String name) {
    this.name = name;
  }
}
