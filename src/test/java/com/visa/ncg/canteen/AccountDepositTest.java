package com.visa.ncg.canteen;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class AccountDepositTest {

  @Test
  public void newAccountShouldHaveZeroBalance() throws Exception {
    // instantiate an account object
    Account account = new Account();
    // check that account has zero balance
    assertThat(account.balance())
        .isZero();
  }

  @Test
  public void deposit10DollarsIntoNewAccountShouldHave10DollarBalance() throws Exception {
    Account account = new Account();

    account.deposit(10);

    assertThat(account.balance())
        .isEqualTo(10);
  }

  @Test
  public void deposit5DollarsAndThen10DollarsResultsIn15DollarBalance() throws Exception {
    Account account = new Account();

    account.deposit(5);
    account.deposit(10);

    assertThat(account.balance())
        .isEqualTo(15);
  }

  @Test
  public void depositNegative5DollarsShouldThrowInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatExceptionOfType(InvalidAmountException.class)
        .isThrownBy(() -> {
            account.deposit(-5);
        });
  }

  @Test
  public void depositOfZeroDollarsShouldThrowInvalidAmountException() throws Exception {
    Account account = new Account();

    assertThatExceptionOfType(InvalidAmountException.class)
        .isThrownBy(() -> {
          account.deposit(0);
        });
  }

}
