package com.visa.ncg.canteen;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AccountNewTest {

  @Test
  public void newAccountHasZeroBalance() throws Exception {
    assertThat(new Account().balance())
        .isZero();
  }

}
