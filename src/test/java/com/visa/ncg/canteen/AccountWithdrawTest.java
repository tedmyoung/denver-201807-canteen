package com.visa.ncg.canteen;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

public class AccountWithdrawTest {

  private Account account;

  @Before
  public void setup() {
    account = new Account();
  }

  @Test
  public void withdraw3DollarsFromAccountHaving7DollarsResultsIn4DollarBalance() throws Exception {
    Account account = new Account();
    account.deposit(7);

    account.withdraw(3);

    assertThat(account.balance())
        .isEqualTo(4);

  }

  @Test
  public void withdraw5Then3ResultsIn12DollarBalance() throws Exception {
    account.deposit(20);

    account.withdraw(5);
    account.withdraw(3);

    assertThat(account.balance())
        .isEqualTo(12);
  }

  @Test
  public void negativeWithdrawalThrowsInvalidAmountException() throws Exception {
    assertThatThrownBy(() -> {
      account.withdraw(-1);
    })
        .isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void zeroWithdrawThrowsInvalidAmountException() throws Exception {
    assertThatThrownBy(() -> {
      account.withdraw(0);
    }).isInstanceOf(InvalidAmountException.class);
  }

  @Test
  public void overdrawingAccountThrowsInsufficientBalanceException() throws Exception {

    account = new Account();

    assertThatThrownBy(() ->
                           account.withdraw(5)
    ).isInstanceOf(InsufficientBalanceException.class);
  }

  @Test
  public void withdraw11DollarsFromAccountWith11DollarBalanceResultsInZeroBalance() throws Exception {
    account.deposit(11);

    account.withdraw(11);

    assertThat(account.balance())
        .isZero();
  }
}
